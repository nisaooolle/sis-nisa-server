package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.Tindakan;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.TindakanService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tindakan")
@CrossOrigin(origins = "http://localhost:3006")
public class TindakanController {

    @Autowired
    TindakanService tindakanService;

    @PostMapping
    public CommonResponse<Tindakan> addtin(@RequestBody Tindakan tindakan) {
        return ResponseHelper.ok(tindakanService.addtin(tindakan));
    }

    @GetMapping("/{id}")
    public CommonResponse<Tindakan> getTinById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.getTinById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<Tindakan> editTinById(@PathVariable("id") Long id,@RequestBody Tindakan tindakan) {
        return ResponseHelper.ok(tindakanService.editTinById(id, tindakan));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteTinById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(tindakanService.deleteTinById(id));
    }

    @GetMapping("/all-tindakan")
    public CommonResponse<List<Tindakan>> getAlltin() {
        return ResponseHelper.ok(tindakanService.getAlltin());
    }
}
