package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.DaftarObat;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DaftarObatService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/daftar-obat")
@CrossOrigin(origins = "http://localhost:3006")
public class DaftarObatController {

    @Autowired
    DaftarObatService daftarObatService;

    @PostMapping
    public CommonResponse<DaftarObat> addobat(@RequestBody DaftarObat daftarObat) {
        return ResponseHelper.ok(daftarObatService.addobat(daftarObat));
    }

    @GetMapping("/{id}")
    public CommonResponse<DaftarObat> getobatById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(daftarObatService.getobatById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<DaftarObat> editobatById(@PathVariable("id") Long id,@RequestBody DaftarObat daftarObat) {
        return ResponseHelper.ok(daftarObatService.editobatById(id, daftarObat));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteobatById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(daftarObatService.deleteobatById(id));
    }

    @GetMapping("/all-obat")
    public CommonResponse<List<DaftarObat>> getAllobat() {
        return ResponseHelper.ok(daftarObatService.getAllobat());
    }
}
