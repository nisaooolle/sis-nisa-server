package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.model.Diagnosa;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DiagnosaService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/diagnosa")
@CrossOrigin(origins = "http://localhost:3006")
public class DiagnosaController {

    @Autowired
    DiagnosaService diagnosaService;

    @PostMapping
    public CommonResponse<Diagnosa> adddiag(@RequestBody Diagnosa diagnosa) {
        return ResponseHelper.ok(diagnosaService.adddiag(diagnosa));
    }

    @GetMapping("/{id}")
    public CommonResponse<Diagnosa> getdiagById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.getdiagById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<Diagnosa> editdiagById(@PathVariable("id") Long id,@RequestBody Diagnosa diagnosa) {
        return ResponseHelper.ok(diagnosaService.editdiagById(id, diagnosa));
    }
    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deletediagById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(diagnosaService.deletediagById(id));
    }

    @GetMapping("/all-diag")
    public CommonResponse<List<Diagnosa>> getAlldiag() {
        return ResponseHelper.ok(diagnosaService.getAlldiag());
    }
}
