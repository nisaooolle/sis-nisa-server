package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sis.ujian.mini.project.Dto.PeriksaPasienDto;
import sis.ujian.mini.project.Dto.TanganiDto;
import sis.ujian.mini.project.model.DaftarObat;
import sis.ujian.mini.project.model.DaftarPasien;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.DaftarPasienService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/daftar-pasien")
@CrossOrigin(origins = "http://localhost:3006")
public class DaftarPasienController {

    @Autowired
    DaftarPasienService daftarPasienService;
//    @Autowired
//    TanganiExcelImpl excelTangani;

//    @GetMapping("/download/")
//    public ResponseEntity<Resource> download_data_pasien(String query, String tanggal) {
//        String filename = "data_pasien.xlsx";
//        InputStreamResource file = new InputStreamResource(excelTangani.loadPasien(query, tanggal));
//
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
//                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
//                .body  (file);
//    }

    @GetMapping
    public CommonResponse<List<DaftarPasien>> allPasien(@RequestParam(name = "query", required = false) String query){
        return ResponseHelper.ok(daftarPasienService.allPasien(query == null ? "": query));
    }

    @GetMapping("/filterTanggal")
    public CommonResponse<List<DaftarPasien>> filterTanggal(@RequestParam(name = "query", required = false) String query, @RequestParam(name = "tanggal", required = false) String tanggal){
        return ResponseHelper.ok(daftarPasienService.filterTanggal(query == null ? "": query, query == null ? "": query));
    }

    @PostMapping
    public CommonResponse<DaftarPasien> addPasien( PeriksaPasienDto siswa ){
        return ResponseHelper.ok(daftarPasienService.addPasien(siswa));
    }

    @PutMapping("/{id}") //untuk mengedit data per id
    public CommonResponse<DaftarPasien> editTangani (@PathVariable("id") Long id,TanganiDto tanganiDto) {
        return ResponseHelper.ok(daftarPasienService.editTangani(id, tanganiDto));
    }

    @GetMapping("/{id}")
    public CommonResponse <DaftarPasien> getId(@PathVariable("id")Long id){
        return ResponseHelper.ok( daftarPasienService.getId(id));
    }


}
