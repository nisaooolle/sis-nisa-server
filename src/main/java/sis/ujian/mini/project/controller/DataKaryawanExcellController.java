package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.DataKaryawanExcel;
import sis.ujian.mini.project.model.ResponseMessage;
import sis.ujian.mini.project.service.KaryawanExcelService;
import sis.ujian.mini.project.service.TemplateKaryawanService;

@RestController
@RequestMapping("/karyawan/excel")
public class DataKaryawanExcellController {

    @Autowired
    KaryawanExcelService fileService;

    @Autowired
    TemplateKaryawanService templateKaryawanService;

    @GetMapping
    public ResponseEntity<Resource> getFile() {
        String filename = "datakaryawan.xlsx";
        InputStreamResource file = new InputStreamResource(fileService.loadKaryawan());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/download/template")
    public ResponseEntity<Resource> getFileTemplate() {
        String filename = "templateKaryawan.xlsx";
        InputStreamResource file = new InputStreamResource(templateKaryawanService.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }


    @PostMapping(path = "/upload/karyawan")
    public ResponseEntity<ResponseMessage> uploadFileKaryawan(@RequestPart("file") MultipartFile file) {
        String message = "";
        if (DataKaryawanExcel.hasExcelFormat(file)) {
            try {
                fileService.saveKaryawan(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body (new ResponseMessage(message));

            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
