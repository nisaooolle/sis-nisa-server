package sis.ujian.mini.project.controller;

import org.hibernate.sql.Update;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.Foto;
import sis.ujian.mini.project.model.Login;
import sis.ujian.mini.project.model.Password;
import sis.ujian.mini.project.model.Registrasi;
import sis.ujian.mini.project.response.CommonResponse;
import sis.ujian.mini.project.response.ResponseHelper;
import sis.ujian.mini.project.service.RegistrasiService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:3006")
public class RegisterController {
    @Autowired
    private RegistrasiService registrasiService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/register")
    public CommonResponse<Registrasi> registrasi(@RequestBody Registrasi sekolah) {
        return ResponseHelper.ok(registrasiService.registrasi(sekolah));
    }

    @PostMapping("/login")
    public CommonResponse<Map<String, Object>> login(@RequestBody Login login) {
        return ResponseHelper.ok(registrasiService.login(login));
    }

    @GetMapping("/{id}")
    public CommonResponse<Registrasi> getById(@PathVariable Long id) {
        return ResponseHelper.ok(registrasiService.getById(id));
    }

    @PutMapping(path = "/{id}")
    public CommonResponse<Registrasi> update(@PathVariable("id") Long id,@RequestBody Update update) {
        return ResponseHelper.ok(registrasiService.update(id, modelMapper.map(update , Registrasi.class)));
    }
    @PutMapping(path = "/password/{id}")
    public CommonResponse<Registrasi> updatePassword(@PathVariable("id") Long id,@RequestBody Password password) {
        return ResponseHelper.ok(registrasiService.updatePassword(id, modelMapper.map(password , Registrasi.class)));
    }
    @PutMapping(path = "/foto/{id}", consumes = "multipart/form-data")
    public CommonResponse<Registrasi> updateFoto(@PathVariable("id") Long id, Foto foto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(registrasiService.updateFoto(id, modelMapper.map(foto , Registrasi.class) , multipartFile));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Boolean>> deleteSekolah(@PathVariable("id") Long id) {
        return ResponseHelper.ok(registrasiService.deleteSekolah(id));
    }

    @GetMapping
    public CommonResponse<List<Registrasi>> getAll() {
        return ResponseHelper.ok(registrasiService.getAll());
    }
}
