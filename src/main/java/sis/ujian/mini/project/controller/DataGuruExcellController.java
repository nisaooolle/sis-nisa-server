package sis.ujian.mini.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.DataGuruExcel;
import sis.ujian.mini.project.model.ResponseMessage;
import sis.ujian.mini.project.service.GuruExcelService;
import sis.ujian.mini.project.service.TemplateGuruService;

@RestController
@RequestMapping("/guru/excel")
public class DataGuruExcellController {

    @Autowired
    GuruExcelService fileService;

    @Autowired
    TemplateGuruService templateGuruService;

    @GetMapping
    public ResponseEntity<Resource> getFile() {
        String filename = "dataguru.xlsx";
        InputStreamResource file = new InputStreamResource(fileService.loadGuru());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/download/template")
    public ResponseEntity<Resource> getFileTemplate() {
        String filename = "templateGuru.xlsx";
        InputStreamResource file = new InputStreamResource(templateGuruService.pExcell());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }


    @PostMapping(path = "/upload/guru")
    public ResponseEntity<ResponseMessage> uploadFileGuru(@RequestPart("file") MultipartFile file) {
        String message = "";
        if (DataGuruExcel.hasExcelFormat(file)) {
            try {
                fileService.saveGuru(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(HttpStatus.OK).body (new ResponseMessage(message));

            } catch (Exception e) {
                System.out.println(e);
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }
}
