package sis.ujian.mini.project.model;

import javax.persistence.*;

@Entity
@Table(name = "daftar_obat")
public class DaftarObat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "namaObat")
    private String NamaObat;

    @Column(name = "stock")
    private int Stock;

    @Column(name = "tgl_exp")
    private String tglExp;

    public DaftarObat() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaObat() {
        return NamaObat;
    }

    public void setNamaObat(String namaObat) {
        NamaObat = namaObat;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int stock) {
        Stock = stock;
    }

    public String getTglExp() {
        return tglExp;
    }

    public void setTglExp(String tglExp) {
        this.tglExp = tglExp;
    }
}
