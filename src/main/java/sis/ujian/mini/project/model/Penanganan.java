package sis.ujian.mini.project.model;

import javax.persistence.*;

@Entity
@Table(name = "penanganan")
public class Penanganan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "namaPenanganan")
    private String NamaPenanganan;

    public Penanganan() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPenanganan() {
        return NamaPenanganan;
    }

    public void setNamaPenanganan(String namaPenanganan) {
        NamaPenanganan = namaPenanganan;
    }
}
