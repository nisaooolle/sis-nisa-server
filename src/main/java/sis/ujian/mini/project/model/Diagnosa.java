package sis.ujian.mini.project.model;


import javax.persistence.*;

@Entity
@Table(name = "diagnosa")
public class Diagnosa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "namaPenyakit")
    private String NamaPenyakit;

    public Diagnosa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPenyakit() {
        return NamaPenyakit;
    }

    public void setNamaPenyakit(String namaPenyakit) {
        NamaPenyakit = namaPenyakit;
    }
}
