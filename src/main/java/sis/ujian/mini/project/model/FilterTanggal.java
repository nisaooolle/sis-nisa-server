package sis.ujian.mini.project.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "filter_tgl")
public class FilterTanggal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "dari_tgl")
    private Date dari;

    @CreationTimestamp
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "sampai_tgl")
    private Date sampai;

    public FilterTanggal() {
    }

    public Date getDari() {
        return dari;
    }

    public void setDari(Date dari) {
        this.dari = dari;
    }

    public Date getSampai() {
        return sampai;
    }

    public void setSampai(Date sampai) {
        this.sampai = sampai;
    }
}
