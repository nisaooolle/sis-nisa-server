package sis.ujian.mini.project.model;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class DataKaryawanExcel {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADERs = {"nama karyawan", "tempat lahir","tanggal lahir","alamat"};
    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static ByteArrayInputStream dataKaryawanToExcel(List<Data> karyawans) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADERs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADERs[col]);
            }

            int rowIdx = 1;
            for (Data dataKaryawan : karyawans) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(dataKaryawan.getUsername());
                row.createCell(1).setCellValue(dataKaryawan.getTempat());
                row.createCell(2).setCellValue(dataKaryawan.getTanggalLahir());
                row.createCell(3).setCellValue(dataKaryawan.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    private static final int Hour = 1000 * 3600;
    public static List<Data> excelToKaryawan(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Data> karyawanList = new ArrayList<Data>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Data dataKaryawan = new Data();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            dataKaryawan.setUsername(currentCell.getStringCellValue());
                            break;
                        case 1:
                            dataKaryawan.setTempat(currentCell.getStringCellValue());
                            break;
                        case 2:
                            dataKaryawan.setTanggalLahir(currentCell.getStringCellValue());
                            break;
                        case 3:
                            dataKaryawan.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    karyawanList.add(dataKaryawan);
                    cellIdx++;

                }
                dataKaryawan.setJabatan("Karyawan");
                dataKaryawan.setStatus("karyawan");
                dataKaryawan.setWaktu((new Date(new Date().getTime() + 24 * 5 * Hour)));
            }
            workbook.close();
            return karyawanList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

}
