package sis.ujian.mini.project.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserPrinciple implements UserDetails {
    private String email;
    private String password;

    private Collection<? extends GrantedAuthority> authority;
    public UserPrinciple(String email, String password) {
        this.email = email;
        this.password = password;
        this.authority = authority;
    }
    public static UserPrinciple build(Registrasi registrasi) {
        return new UserPrinciple(
                registrasi.getEmail(),
                registrasi.getPassword()
        );
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }
    @Override
    public String getPassword() {
        return password;
    }
    @Override
    public String getUsername() {
        return email;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }



}
