package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.Dto.PeriksaPasienDto;
import sis.ujian.mini.project.Dto.TanganiDto;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.DaftarPasien;
import sis.ujian.mini.project.repository.*;
import sis.ujian.mini.project.service.DaftarPasienService;

import java.util.List;

@Service
public class DaftarPasienlmpl implements DaftarPasienService {

    @Autowired
    DaftarPasienRepository daftarPasienRepository;

    @Autowired
    DataRepository dataRepository;

    @Autowired
    DiagnosaRepository diagnosaRepository;

    @Autowired
    PenangananRepository penangananRepository;

    @Autowired
    TindakanRepository tindakanRepository;

    @Override
    public List<DaftarPasien> allPasien(String query) {
        return daftarPasienRepository.findAll();
    }

    @Override
    public DaftarPasien addPasien(PeriksaPasienDto periksaPasien) {
        DaftarPasien periksaPasien1 = new DaftarPasien();
        periksaPasien1.setKeluhan(periksaPasien.getKeluhan());
        periksaPasien1.setStatusPasien(periksaPasien.getStatusPasien());
        periksaPasien1.setStatus("belum ditangani");
        periksaPasien1.setNamaPasien(dataRepository.findById(periksaPasien.getNamaPasien()).orElseThrow(() -> new NotFoundException("id not found")));
        return daftarPasienRepository.save(periksaPasien1);
    }

    @Override
    public DaftarPasien editTangani(Long id, TanganiDto tanganiDto) {
        DaftarPasien periksaPasien = daftarPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("Not Found"));
        periksaPasien.setStatus("sudah ditangani");
        periksaPasien.setPenyakitId(diagnosaRepository.findById(tanganiDto.getPanyakitId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setPenangananId(penangananRepository.findById(tanganiDto.getPenangananId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setTindakanId(tindakanRepository.findById(tanganiDto.getTindakanId()).orElseThrow(() -> new NotFoundException("id not found")));
        periksaPasien.setKeluhan(tanganiDto.getKeluhan());
        return daftarPasienRepository.save(periksaPasien);
    }

    @Override
    public DaftarPasien getId(Long id) {
        var periksaPasien = daftarPasienRepository.findById(id).get();
        return daftarPasienRepository.save(periksaPasien);
    }

    @Override
    public List<DaftarPasien> filterTanggal(String query, String tanggal) {
        return daftarPasienRepository.filterTanggal(query, tanggal);
    }

}
