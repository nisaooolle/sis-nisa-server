package sis.ujian.mini.project.lmpl;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.exception.InternalErrorException;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.jwt.JwtProvider;
import sis.ujian.mini.project.model.Login;
import sis.ujian.mini.project.model.Registrasi;
import sis.ujian.mini.project.repository.RegistrasiRepository;
import sis.ujian.mini.project.service.RegistrasiService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class Registerlmpl implements RegistrasiService {
    @Autowired
    private RegistrasiRepository registrasiRepository;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email or Password not found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Registrasi registrasi(Registrasi registrasi) {
        String UserPassword = registrasi.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(registrasi.getEmail()).matches();
        if (!isEmail)throw new InternalErrorException("Email Not Valid");
//        if (sekolah.getRole().name().equals("ADMIN")) {
//            sekolah.setRole(RoleEnum.ADMIN);
//        } else if (sekolah.getRole().name().equals("GURU")) {
//            sekolah.setRole(RoleEnum.GURU);
//        } else if (sekolah.getRole().name().equals("PERPUSTAKAAN")) {
//            sekolah.setRole(RoleEnum.PERPUSTAKAAN);
//        } else if (sekolah.getRole().name().equals("TU")) {
//            sekolah.setRole(RoleEnum.TU);
//        } else if (sekolah.getRole().name().equals("AKADEMIK")) {
//            sekolah.setRole(RoleEnum.AKADEMIK);
//        } else if (sekolah.getRole().name().equals("SUPERADMIN")) {
//            sekolah.setRole(RoleEnum.SUPERADMIN);
//        }
        registrasi.setPassword(passwordEncoder.encode(registrasi.getPassword()));
        return registrasiRepository.save(registrasi);
    }

    @Override
    public Map<String, Object> login(Login login) {
        String token = authories(login.getEmail(), login.getPassword());
        Registrasi registrasi ;

//        mengecek email
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(login.getEmail()).matches();
        System.out.println("is Email " + isEmail);

//        jika true, akan menjalankan sistem if
        if(isEmail) {
            registrasi = registrasiRepository.findByEmail(login.getEmail());
        } else { // jika false, else akan dijalankan, dgn login username
            registrasi = registrasiRepository.findByUsername(login.getEmail());
        }

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", registrasi);
        return response;
    }

    @Override
    public Registrasi getById(Long id) {
        return registrasiRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));
    }

    @Override
    public Registrasi update(Long id, Registrasi registrasi) {
        Registrasi update = registrasiRepository.findById(id).orElseThrow(() -> new NotFoundException("Id not found"));
        update.setEmail(registrasi.getEmail());
        boolean isEmail = Pattern.compile("^(.+)@(\\S+)$")
                .matcher(registrasi.getEmail()).matches();
        if (!isEmail)throw new InternalErrorException("Email Not Valid");
        return registrasiRepository.save(update);
    }

    @Override
    public Registrasi updatePassword(Long id, Registrasi registrasi) {
        Registrasi update = registrasiRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        String UserPassword = registrasi.getPassword().trim();
        boolean PasswordIsNotValid = !UserPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,20}");
        if (PasswordIsNotValid) throw new InternalErrorException("Password not valid!");
        update.setPassword(passwordEncoder.encode(registrasi.getPassword()));
        return registrasiRepository.save(update);
    }

    @Override
    public Registrasi updateFoto(Long id, Registrasi registrasi, MultipartFile multipartFile) {
        Registrasi update = registrasiRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not found"));
        String url = convertToBase64(multipartFile);
        update.setFoto(url);
        return registrasiRepository.save(update);
    }

    private String convertToBase64(MultipartFile file) {
        String url = "";
        try {
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String res = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + res;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return url;
        }
    }

    @Override
    public Map<String, Boolean> deleteSekolah(Long id) {
        try {
            registrasiRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }

    @Override
    public List<Registrasi> getAll() {
        return registrasiRepository.findAll();
    }

}
