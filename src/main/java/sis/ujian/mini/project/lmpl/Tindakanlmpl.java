package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.Tindakan;
import sis.ujian.mini.project.repository.TindakanRepository;
import sis.ujian.mini.project.service.TindakanService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Tindakanlmpl implements TindakanService {

    @Autowired
    TindakanRepository tindakanRepository;

    @Override
    public Tindakan addtin(Tindakan tindakan) {
        Tindakan tin = new Tindakan();
        tin.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tin);
    }

    @Override
    public Tindakan getTinById(Long id) {
        return tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<Tindakan> getAlltin() {
        return tindakanRepository.findAll();
    }

    @Override
    public Tindakan editTinById(Long id, Tindakan tindakan) {
        Tindakan tin = tindakanRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        tin.setNamaTindakan(tindakan.getNamaTindakan());
        return tindakanRepository.save(tin);
    }

    @Override
    public Map<String, Boolean> deleteTinById(Long id) {
        try {
            tindakanRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
