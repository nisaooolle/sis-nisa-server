package sis.ujian.mini.project.lmpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.exception.NotFoundException;
import sis.ujian.mini.project.model.Penanganan;
import sis.ujian.mini.project.repository.PenangananRepository;
import sis.ujian.mini.project.service.PenangananService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Penangananlmpl implements PenangananService {

    @Autowired
    PenangananRepository penangananRepository;

    @Override
    public Penanganan addpena(Penanganan penanganan) {
        Penanganan pena = new Penanganan();
        pena.setNamaPenanganan(penanganan.getNamaPenanganan());
        return penangananRepository.save(pena);
    }

    @Override
    public Penanganan getpenaById(Long id) {
        return penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
    }

    @Override
    public List<Penanganan> getAllpena() {
        return penangananRepository.findAll();
    }

    @Override
    public Penanganan editpenaById(Long id, Penanganan penanganan) {
        Penanganan pena = penangananRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Yang Ingin Di Edit Tidak Ada"));
        pena.setNamaPenanganan(penanganan.getNamaPenanganan());
        return penangananRepository.save(pena);
    }

    @Override
    public Map<String, Boolean> deletepenaById(Long id) {
        try {
            penangananRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
