package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.model.TemplateKaryawan;
import sis.ujian.mini.project.repository.DataRepository;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateKaryawanService {

    @Autowired
    DataRepository dataRepository;


    public ByteArrayInputStream pExcell() {
        List<Data> karyawanList = dataRepository.findAll();
        ByteArrayInputStream in = TemplateKaryawan.templateKarToExcel(karyawanList);
        return in;
    }
}
