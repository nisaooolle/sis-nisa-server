package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.Diagnosa;

import java.util.List;
import java.util.Map;

public interface DiagnosaService {

    Diagnosa adddiag(Diagnosa diagnosa); //method add untuk mengepost

    Diagnosa getdiagById(Long id); // method untuk melihat sesuaiid yg dicari

    List<Diagnosa> getAlldiag(); // method untuk melihat semua data

    Diagnosa editdiagById(Long id, Diagnosa diagnosa); // method untuk mengedit data sesuai id

    Map<String, Boolean> deletediagById(Long id); // method untuk mendelete data sesaui data
}
