package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.model.Template;
import sis.ujian.mini.project.repository.DataRepository;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateSiswaService {

    @Autowired
    DataRepository dataRepository;


    public ByteArrayInputStream pExcell() {
        List<Data> siswasList = dataRepository.findAll();
        ByteArrayInputStream in = Template.templateToExcel(siswasList);
        return in;
    }
}
