package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.DaftarObat;

import java.util.List;
import java.util.Map;

public interface DaftarObatService {

    DaftarObat addobat(DaftarObat daftarObat); //method add untuk mengepost

    DaftarObat getobatById(Long id); // method untuk melihat sesuaiid yg dicari

    List<DaftarObat> getAllobat(); // method untuk melihat semua data

    DaftarObat editobatById(Long id, DaftarObat daftarObat); // method untuk mengedit data sesuai id

    Map<String, Boolean> deleteobatById(Long id); // method untuk mendelete data sesaui data

}
