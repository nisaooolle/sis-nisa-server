
package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.*;
import sis.ujian.mini.project.repository.DataRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class KaryawanExcelService {

    @Autowired
    DataRepository dataRepository;

    public ByteArrayInputStream loadKaryawan() {
        List<Data> karyawans = dataRepository.getAllKaryawan();
        ByteArrayInputStream in = DataKaryawanExcel.dataKaryawanToExcel(karyawans);
        return in;
    }

    public void saveKaryawan(MultipartFile file) {
        try {
            List<Data> karyawans = DataKaryawanExcel.excelToKaryawan(file.getInputStream());
            dataRepository.saveAll(karyawans);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
