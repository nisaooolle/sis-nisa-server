package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.Penanganan;

import java.util.List;
import java.util.Map;

public interface PenangananService {

    Penanganan addpena(Penanganan penanganan); //method add untuk mengepost

    Penanganan getpenaById(Long id); // method untuk melihat sesuaiid yg dicari

    List<Penanganan> getAllpena(); // method untuk melihat semua data

    Penanganan editpenaById(Long id, Penanganan penanganan); // method untuk mengedit data sesuai id

    Map<String, Boolean> deletepenaById(Long id); // method untuk mendelete data sesaui data

}
