package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sis.ujian.mini.project.model.*;
import sis.ujian.mini.project.repository.DataRepository;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class TemplateGuruService {

    @Autowired
    DataRepository dataRepository;


    public ByteArrayInputStream pExcell() {
        List<Data> guruList = dataRepository.findAll();
        ByteArrayInputStream in = TemplateGuru.templateGuruToExcel(guruList);
        return in;
    }
}
