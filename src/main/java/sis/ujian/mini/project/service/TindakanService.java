package sis.ujian.mini.project.service;

import sis.ujian.mini.project.model.Tindakan;

import java.util.List;
import java.util.Map;

public interface TindakanService {

    Tindakan addtin(Tindakan tindakan); //method add untuk mengepost

    Tindakan getTinById(Long id); // method untuk melihat sesuaiid yg dicari

    List<Tindakan> getAlltin(); // method untuk melihat semua data

    Tindakan editTinById(Long id, Tindakan tindakan); // method untuk mengedit data sesuai id

    Map<String, Boolean> deleteTinById(Long id); // method untuk mendelete data sesaui data

}
