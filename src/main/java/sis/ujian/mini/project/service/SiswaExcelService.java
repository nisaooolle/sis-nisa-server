
package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.model.DataSiswaExcel;
import sis.ujian.mini.project.repository.DataRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class SiswaExcelService {

    @Autowired
    DataRepository dataRepository;

    public ByteArrayInputStream loadSiswa() {
        List<Data> siswas = dataRepository.getAllSiswa();
        ByteArrayInputStream in = DataSiswaExcel.dataSiswaToExcel(siswas);
        return in;
    }

    public void saveSiswa(MultipartFile file) {
        try {
            List<Data> siswas = DataSiswaExcel.excelToSiswa(file.getInputStream());
            dataRepository.saveAll(siswas);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
