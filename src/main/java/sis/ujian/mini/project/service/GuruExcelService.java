
package sis.ujian.mini.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.Data;
import sis.ujian.mini.project.model.DataGuruExcel;
import sis.ujian.mini.project.repository.DataRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Service
public class GuruExcelService {

    @Autowired
    DataRepository dataRepository;

    public ByteArrayInputStream loadGuru() {
        List<Data> gurus = dataRepository.getAllGuru();
        ByteArrayInputStream in = DataGuruExcel.dataGuruToExcel(gurus);
        return in;
    }

    public void saveGuru(MultipartFile file) {
        try {
            List<Data> gurus = DataGuruExcel.excelToGuru(file.getInputStream());
            dataRepository.saveAll(gurus);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}
