package sis.ujian.mini.project.service;

import org.springframework.web.multipart.MultipartFile;
import sis.ujian.mini.project.model.Login;
import sis.ujian.mini.project.model.Registrasi;

import java.util.List;
import java.util.Map;

public interface RegistrasiService {
    Registrasi registrasi(Registrasi registrasi);

    Map<String, Object> login(Login login);

    Registrasi getById(Long id);

    Registrasi update(Long id, Registrasi registrasi);

    Registrasi updatePassword(Long id, Registrasi registrasi);

    Registrasi updateFoto(Long id, Registrasi registrasi, MultipartFile multipartFile);

    Map<String, Boolean> deleteSekolah(Long id);

    List<Registrasi> getAll();

}
