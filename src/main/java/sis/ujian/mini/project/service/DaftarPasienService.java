package sis.ujian.mini.project.service;

import sis.ujian.mini.project.Dto.PeriksaPasienDto;
import sis.ujian.mini.project.Dto.TanganiDto;
import sis.ujian.mini.project.model.DaftarPasien;

import java.util.List;
import java.util.Map;

public interface DaftarPasienService {
    List<DaftarPasien> allPasien(String query);

    DaftarPasien addPasien(PeriksaPasienDto periksaPasien);

    DaftarPasien editTangani(Long id, TanganiDto tanganiDto);

    DaftarPasien getId(Long id);

    List<DaftarPasien> filterTanggal(String query, String tanggal);

}
