package sis.ujian.mini.project.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import sis.ujian.mini.project.exception.InternalErrorException;
import sis.ujian.mini.project.model.Registrasi;
import sis.ujian.mini.project.model.TemporaryToken;
import sis.ujian.mini.project.repository.RegistrasiRepository;
import sis.ujian.mini.project.repository.TokenRepository;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    private static String secretKey = "Token";
    private static Integer expired = 900000;
    @Autowired
    private TokenRepository temporaryTokenRepository;
    @Autowired
    private RegistrasiRepository registrasiRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
        Registrasi user = registrasiRepository.findByEmail(userDetails.getUsername());
        var checkingToken = temporaryTokenRepository.findByRegisterId(user.getId());
        if (checkingToken.isPresent()) temporaryTokenRepository.deleteById(checkingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setRegisterId(user.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token error parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token expired");
            return false;
        }
        return true;
    }

}
