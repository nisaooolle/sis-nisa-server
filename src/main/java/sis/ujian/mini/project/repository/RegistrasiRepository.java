package sis.ujian.mini.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import sis.ujian.mini.project.model.Registrasi;


public interface RegistrasiRepository extends JpaRepository<Registrasi, Long> {

    Registrasi findByEmail(String email);

//    @Query(value = "SELECT * FROM sekolah  WHERE " +
//            "email LIKE CONCAT('%',:query, '%')", nativeQuery = true)
//    List<Registrasi> findAll(String query, Pageable pageable);

    @Query(value = "SELECT * FROM register  WHERE " +
            "username LIKE CONCAT('%',:username, '%')", nativeQuery = true)
    Registrasi findByUsername(String username);
}
