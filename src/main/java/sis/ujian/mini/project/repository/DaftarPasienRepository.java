package sis.ujian.mini.project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sis.ujian.mini.project.model.DaftarPasien;

import java.util.List;

@Repository
public interface DaftarPasienRepository extends JpaRepository<DaftarPasien, Long> {
//    @Query(value = "SELECT * FROM daftar_pasien WHERE nama_pasien LIKE CONCAT ('%',:query,'%')", nativeQuery = true)
//    List<DaftarPasien> allPasien (Long id);

//    @Query(value = "SELECT p.* FROM daftar-pasien p WHERE " +
//            "p.id LIKE CONCAT('%',:query, '%')", nativeQuery = true)
//    List<DaftarPasien> findByIdDaftarPasien(DaftarPasien daftarPasien);

    @Query(value = "SELECT * FROM daftar_pasien WHERE tanggal LIKE CONCAT ('%', :query, '%') AND tanggal LIKE CONCAT ('%', :tanggal, '%')", nativeQuery = true)
    List<DaftarPasien> filterTanggal(String query, String tanggal);

}
