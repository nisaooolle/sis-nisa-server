package sis.ujian.mini.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sis.ujian.mini.project.model.Diagnosa;

@Repository
public interface DiagnosaRepository extends JpaRepository<Diagnosa, Long> {
}
